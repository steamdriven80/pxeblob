## PXEBlob -  A working PXE-Boot template

So I don't know about you, but PXE has always been a very lofty goal in my personal cloud, in-my-head.buzz.  Whenever I needed one, I'd usually be in a crisis with a crashed machine and I'm trying to get it up and running as fast as possible, running around with USB keys, CDs, finding apparently corrupted memory, drivers, and pockets of bad luck everywhere!  A PXE server would've been PERFECT, if only the thing had instructions from the past decdate  I'd always be in a hurry or easily distracted and inevitably miss a step or misconfigure an IP address.

So it'd be put off some more; once I had the machine up again, who needs the PXE server?  Right...  Well, I was playing with CoreOS a bit and noticed some really easy instructions for a network boot...

So here's a complete example of a working DHCP/PXE server!  There are a few key items you'd need before it'll start, however:

1.  Clone the repo from GitHub:

	```bash
	$ git clone https://github.com/gearmover/pxeblob
	```
	
2.  cd in and pull down whatever OS images you'd like:

	```bash
	$ cd pxeblob/tftpboot/
	$ BASE="http://stable.release.core-os.net/amd64-usr/current/"

	$ wget "$BASE/coreos_production_pxe.vmlinuz"
	$ wget "$BASE/coreos_production_pxe_image.cpio.gz"
 	```
 	
3.  [Optional]: Verify the images using CoreOS's keys:  
	Source and great read: https://coreos.com/docs/running-coreos/bare-metal/booting-with-pxe/

	```bash    
	$ wget "$BASE/coreos_production_pxe.vmlinuz.sig"
	$ wget "$BASE/coreos_production_pxe_image.cpio.gz.sig"
	$ gpg --verify coreos_production_pxe.vmlinuz.sig
	$ gpg --verify coreos_production_pxe_image.cpio.gz.sig
	```
	
4.  Modify the Cloud-Config script in pxeblob/tftpboot/http/pxe-cloud-config.yml  

	```yaml
	#cloud-config
	coreos:
	  units: 
	    - name: etcd.service
	      command: start
	    - name: fleet.service
	      command: start
	ssh_authorized_keys:
	  - ssh-rsa ## YOUR SSH.pub PUBLIC KEY HERE  ## chris@chr1s.co
	```
	
5.  This file can be served a variety of ways but HTTP is fairly straightfoward.  
    A simple example would be:

	Using NODEJS:
	
	```bash
	$ npm install -g http-server
	$ sudo hs pxeblob/http/ -p80
	```

6.  Modify the 'default' pxe linux config file located at 
    pxeblob/tftpboot/pxelinux.cfg/default to reference your HTTP server in the 
    kernel boot options command line.

7.  I've included an example DNSMasq configuration file (from /etc/dnsmasq.cfg 
    or optionally passed on the command line) and other DNS servers would need 
    to send very similar options:

	```yaml
   		next-server IP-ADDRESS-OF-TFTP-SERVER (prob same as DHCP server)
   		filename	pxelinux.0
	```
	
Good luck, and hope you can expand this small example to whatever operating 
systems/images you want to serve over your own network.  

Improvements or suggestions or complaints always welcome!  Thanks for checking this project out.
